# Zander's Dotfiles

These are my configs to make my systems comfortable for me. This repo is specifically for a laptop setup. My current setup is powered by Hyprland. I've used other compositors window managers before (AwesomeWM mostly) which I've kept on other branches.

Dependencies:
- Hyprland: Compositor
- Waybar: Status bar
- Hyprpaper: Wallpaper
- Dunst: Notification daemon
- Anyrun: Launcher
- ZSH: Shell
- Kitty: Terminal emulator

Notable contents:
- **Hyprland:**
	- `.config/hypr/hyprland.conf`
- **Hyprpaper:**
	- `.config/hypr/hyprpaper.conf`
- **Waybar:** 
	- `.config/waybar/config.jsonc`
	- `.config/waybar/style.css`
	- Notification module:
		- `.config/waybar/scripts/dusnt.sh`
			- Used to display appropriate notification icon on the notification Waybar module.
	- Loadshedding module: 
		- `.config/waybar/loadsheddicons/`
			- Contains icons for the loadshedding Waybar module.
		- `.config/waybar/scripts/loadshedding/`
			- Loadshedding Waybar module. Powered by the EskomCalendar API: https://github.com/beyarkay/eskom-calendar 
- **Anyrun:**
	- `.config/anyrun/config.ron`
	- `.config/anyrun/style.css`
- **ZSH:**
	- `.zshrc`
- **Dunst:**
	- `.config/dunst/dunstrc`
- **Kitty:**
	- `.config/kitty/kitty.conf`
	- `.config/kitty/colors.conf`