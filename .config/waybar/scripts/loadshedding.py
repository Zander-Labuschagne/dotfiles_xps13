#!/usr/bin/env python

import subprocess
import requests
from datetime import datetime, timedelta
import json
from twisted.internet import task, reactor
import sys
from string import Template
from plyer import notification

area_id = "capetown-8-caprivillage"
# Get your own API token here: https://eskomsepush.gumroad.com/l/api
token = "YOUR_TOKEN"
url = "https://developer.sepush.co.za/business/2.0/area?id=" + area_id
api_call_period = 60  # 60 minutes (24 calls a day + updates)
widget_loop_period = 60  # loop every minute (60 s)
jsonResponse = ""
persistent_state = 0
notified_stage = "0"

i = 0

loadshedding_icons = {
    "none": "",
    # "none": "",
    # "none": "",
    "days": "",
    "tomorrow": "",
    "later": "",
    "minutes": "",
    "sheddingALoad": "",
}

loadshedding_icon_files = {
    "none": "powered.svg",
    "days": "warn.svg",
    "tomorrow": "warn.svg",
    "later": "warn.svg",
    "minutes": "attention_red.svg",
    "sheddingALoad": "shedding.svg",
}

stage_icons = {
    "0": "",
    "1": "",
    "2": "",
    "3": "",
    "4": "",
    "5": "",
    "6": "",
    "7": "",
    "8": "",
}

stage_files = {
    "0": "s0.svg",
    "1": "s1.svg",
    "2": "s2.svg",
    "3": "s3.svg",
    "4": "s4.svg",
    "5": "s6.svg",
    "6": "s6.svg",
    "7": "s7.svg",
    "8": "s8.svg",
}

class DeltaTemplate(Template):
    delimiter = "%"


def strfdelta(tdelta, fmt):
    h = tdelta.seconds // 3600
    d = {"H": f"{h:02}"}
    rem = tdelta.seconds % 3600
    m = rem // 60
    d["M"] = f"{m:02}"
    s = rem % 60
    d["S"] = f"{s:02}"
    t = DeltaTemplate(fmt)
    return t.substitute(**d)


def widget_function():
    global i
    global jsonResponse
    global persistent_state
    global notified_stage

    if i <= 0:
        response = requests.get(url, headers={"token": token})
        jsonResponse = response.json()
        i = api_call_period * 60

    i -= widget_loop_period
    state = 0
    stage = "0"
    # print(jsonResponse)

    # Check that there actually are events, maybe loadshedding has ended once
    # and for all and there will be no events.
    if len(jsonResponse["events"]) > 0:
        state = 1
    else:  # No loadshedding
        status_code = "none"
        text = "No loadshedding"
        tooltip_header = "No loadshedding!"
        tooltip_note = ""
        notified_stage = "0"
        notification_state = 0

    if state == 1:
        nextEvent = jsonResponse["events"][0]

        start = datetime.fromisoformat(nextEvent["start"])
        time = start.time()
        end = datetime.fromisoformat(nextEvent["end"])
        note = nextEvent["note"]
        now = datetime.now()

        minutes55 = start - timedelta(minutes=55)

        if now.timestamp() < minutes55.timestamp():
            state = 1
        elif now.timestamp() >= minutes55.timestamp() and now.timestamp() < start.timestamp():
            state = 2
        elif now.timestamp() >= start.timestamp() and now.timestamp() <= end.timestamp():
            state = 3
        # else:
            # Loadshedding has passed, TODO: get the next one

        delta = start.date() - now.date()

    # If further in advance of one day, get the day of week
    # (hopefully we don't have to look further than one week, hopefully?)
    if state > 0 and delta.days > 1:
        status_code = "days"
        text = str(start.date().weekday())
        tooltip_header = "Loadshedding on " + text + " at " + time.strftime("%H:%M")
        notification_state = 1
    # Check if loadshedding is tomorrow
    elif state > 0 and delta.days > 0:
        status_code = "tomorrow"
        text = "Tomorrow " + time.strftime("%H:%M")
        tooltip_header = "Loadshedding tomorrow at " + time.strftime("%H:%M")
        notification_state = 2
    elif state == 1:
        status_code = "later"
        text = time.strftime("%H:%M")
        tooltip_header = "Loadshedding at " + time.strftime("%H:%M")
        notification_state = 3
    elif state == 2:
        status_code = "minutes"
        countdown = start.replace(tzinfo=None) - now.replace(tzinfo=None)
        text = strfdelta(countdown, "%H:%M")
        tooltip_header = "Loadshedding at " + time.strftime("%H:%M")
        notification_state = 4
    elif state == 3:
        status_code = "sheddingALoad"
        countdown = end.replace(tzinfo=None) - now.replace(tzinfo=None)
        text = strfdelta(countdown, "%H:%M")
        tooltip_header = "Loadshedding is over at " + end.time().strftime("%H:%M")
        notification_state = 5

    if state > 0:
        note = str(nextEvent["note"])
        if note.startswith("Stage "):
            # Note: Currently only anticipating single digit stages...
            stage = note[-1]
            tooltip_note = stage_icons[stage] + " " + note
        else:
            tooltip_note = note
    icon = loadshedding_icons[status_code]
    tooltip = str.format(
        "\t\t{}\t\t\n\t\t\n{}\n\n{}",
        f'<span size="xx-large">{icon}</span>',
        f"<big>{tooltip_header}</big>",
        f"<big>{tooltip_note}</big>",
    )

    # Push notification if the stage has changed.
    if notified_stage != stage:
        if stage == 0:
            notification_text = "Loadshedding suspended"
        else:
            notification_text = "Loadshedding " + note + " implemented"
        notification.notify(
            title = notification_text,
            timeout = 10,
            app_icon = "~/.config/waybar/loadsheddicons/" + stage_files[stage]
        )
        notified_stage = stage

    # Push notification if the state has changed
    if persistent_state != notification_state:
        if state == 2:
            notification_title = "Loadshedding in 55 minutes"
        elif state == 3:
            notification_title = "Loadshedding has started"
        else:
            notification_title = tooltip_header
        notification.notify(
            title = notification_title,
            message = note,
            timeout = 10,
            app_icon = "~/.config/waybar/loadsheddicons/" + loadshedding_icon_files[status_code]
        )
        persistent_state = notification_state
        # Make the API call again for the latest update
        i = 0
        # But run immediately to have a quick update.
        # Should not get into recursive loop because the persistent state
        # has changed and should now match the local state.
        # widget_function()

    # print waybar module data
    out_data = {
        "text": f"{icon} {text}",
        "alt": tooltip_note,
        "tooltip": tooltip,
        "class": status_code,
    }
    sys.stdout.write(json.dumps(out_data) + "\n")
    sys.stdout.flush()


widget_loop = task.LoopingCall(widget_function)
widget_loop.start(widget_loop_period)
reactor.run()
