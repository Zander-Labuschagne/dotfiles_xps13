use std::thread::sleep;
use std::time::Duration;
use serde::{Serialize, Deserialize};
use chrono::{DateTime, Datelike, FixedOffset, TimeDelta};
use notify_rust::Notification;

static AREA_NAME: &str = "city-of-cape-town-area-8";
static API_URL: &str = "https://eskom-calendar-api.shuttleapp.rs/outages/";
static API_CALL_PERIOD_MIN: u8 = 60;  // 60 minutes (24 calls a day + updates)
static WIDGET_LOOP_PERIOD_S: u16 = 60;  // loop every minute (60 s)
static SOON: TimeDelta = TimeDelta::minutes(55);
static ICONS_DIRECTORY: &str = "~/.config/waybar/loadsheddicons/";

#[derive(Serialize, Deserialize)]
struct Loadshedding { // Eskom Calendar API JSON Response
    area_name: String,
    start: String,
    finsh: String,
    stage: u8,
    source: String,
}

#[derive(Serialize)]
struct Widget { // Widget JSON Output
    text: String,
    alt: String,
    tooltip: String,
    class: String
}

impl Widget {
    pub fn from(loadshedding_state: &LoadsheddingState, stage: &Stage) -> Widget {
        
        let note: String = stage.get_unicode_icon() + " Stage " + stage.ordinal().to_string().as_str();

        match loadshedding_state {
            LoadsheddingState::None => return Widget {
                text: String::from(loadshedding_state.get_unicode_icon()) + " No loadshedding",
                tooltip: format_tooltip(&loadshedding_state.get_unicode_icon(), &loadshedding_state.get_tooltip_header(), ""),
                alt: String::from(""),
                class: String::from("none"),
            },
            LoadsheddingState::Days(day, time) => return Widget {
                text: String::from(loadshedding_state.get_unicode_icon()) + " " + day + " " + time,
                tooltip: format_tooltip(&loadshedding_state.get_unicode_icon(), &loadshedding_state.get_tooltip_header(), &note),
                alt: note,
                class: String::from("days"),
            },
            LoadsheddingState::Tomorrow(time) => return Widget {
                text: String::from(loadshedding_state.get_unicode_icon()) + " Tomorrow " + time,
                tooltip: format_tooltip(&loadshedding_state.get_unicode_icon(), &loadshedding_state.get_tooltip_header(), &note),
                alt: note,
                class: String::from("tomorrow"),
            },
            LoadsheddingState::Later(time) => return Widget {
                text: String::from(loadshedding_state.get_unicode_icon()) + " " + time,
                tooltip: format_tooltip(&loadshedding_state.get_unicode_icon(), &loadshedding_state.get_tooltip_header(), &note),
                alt: note,
                class: String::from("later"),
            },
            LoadsheddingState::Minutes(_, countdown) => return Widget {
                text: String::from(loadshedding_state.get_unicode_icon()) + " " + countdown,
                tooltip: format_tooltip(&loadshedding_state.get_unicode_icon(), &loadshedding_state.get_tooltip_header(), &note),
                alt: note,
                class: String::from("minutes"),
            },
            LoadsheddingState::SheddingALoad(_, remaining) => return Widget {
                text: String::from(loadshedding_state.get_unicode_icon()) + " " + remaining,
                tooltip: format_tooltip(&loadshedding_state.get_unicode_icon(), &loadshedding_state.get_tooltip_header(), &note),
                alt: note,
                class: String::from("sheddingALoad"),
            },
        }
    }
}

#[derive(PartialEq)]
enum LoadsheddingState {
    None,
    Days(String, String),
    Tomorrow(String),
    Later(String),
    Minutes(String, String),
    SheddingALoad(String, String),
}

impl LoadsheddingState {
    pub fn get_unicode_icon(self: &LoadsheddingState) -> String {
        match self {
            LoadsheddingState::None => return String::from(""),
            LoadsheddingState::Days(_, _) => return String::from(""),
            LoadsheddingState::Tomorrow(_) => return String::from(""),
            LoadsheddingState::Later(_) => return String::from(""),
            LoadsheddingState::Minutes(_, _) => return String::from(""),
            LoadsheddingState::SheddingALoad(_, _) => return String::from(""),
        }
    }

    pub fn get_icon_file(self: &LoadsheddingState) -> String {
        match self {
            LoadsheddingState::None => return String::from("powered.svg"),
            LoadsheddingState::Days(_, _) => return String::from("warn.svg"),
            LoadsheddingState::Tomorrow(_) => return String::from("warn.svg"),
            LoadsheddingState::Later(_) => return String::from("warn.svg"),
            LoadsheddingState::Minutes(_, _) => return String::from("attention_red.svg"),
            LoadsheddingState::SheddingALoad(_, _) => return String::from("shedding.svg"),
        }
    }

    pub fn get_tooltip_header(self: &LoadsheddingState) -> String {
        match self {
            LoadsheddingState::None => return String::from("No loadshedding!"),
            LoadsheddingState::Days(day, time) => return String::from("Loadshedding on ") + day + " at " + time,
            LoadsheddingState::Tomorrow(time) => return String::from("Loadshedding tomorrow at ") + time,
            LoadsheddingState::Later(time) => return String::from("Loadshedding at ") + time,
            LoadsheddingState::Minutes(time, _) => return String::from("Loadshedding at ") + time,
            LoadsheddingState::SheddingALoad(time, _) => return String::from("Loadshedding is over at ") + time,
        }
    } 
}

#[derive(Eq, PartialEq)]
enum Stage {
    None,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight
}

impl Stage {
    pub fn get_unicode_icon(self: &Stage) -> String {
        match self {
            Stage::None => return String::from(""),
            Stage::One => return String::from(""),
            Stage::Two => return String::from(""),
            Stage::Three => return String::from(""),
            Stage::Four => return String::from(""),
            Stage::Five => return String::from(""),
            Stage::Six => return String::from(""),
            Stage::Seven => return String::from(""),
            Stage::Eight => return String::from(""),
        }
    }

    pub fn get_icon_file(self: &Stage) -> String {
        match self {
            Stage::None => return String::from("s0.svg"),
            Stage::One => return String::from("s1.svg"),
            Stage::Two => return String::from("s2.svg"),
            Stage::Three => return String::from("s3.svg"),
            Stage::Four => return String::from("s4.svg"),
            Stage::Five => return String::from("s5.svg"),
            Stage::Six => return String::from("s6.svg"),
            Stage::Seven => return String::from("s7.svg"),
            Stage::Eight => return String::from("s8.svg"),
        }
    }

    pub fn ordinal(self: &Stage) -> u8 {
        match self {
            Stage::None => return 0,
            Stage::One => return 1,
            Stage::Two => return 2,
            Stage::Three => return 3,
            Stage::Four => return 4,
            Stage::Five => return 5,
            Stage::Six => return 6,
            Stage::Seven => return 7,
            Stage::Eight => return 8,
        }
    }

    pub fn get(stage: u8) -> Stage {
        match stage {
            0 => return Stage::None,
            1 => return Stage::One,
            2 => return Stage::Two,
            3 => return Stage::Three,
            4 => return Stage::Four,
            5 => return Stage::Five,
            6 => return Stage::Six,
            7 => return Stage::Seven,
            8 => return Stage::Eight,
            _ => panic!("Invalid stage")
        }
    }
}

fn main() {
    let mut notified_state = LoadsheddingState::None;
    let mut notified_stage = Stage::None;
    let mut next_loadshedding_start_time: Option<DateTime<chrono::Local>> = Option::None;
    let mut next_loadshedding_end_time: Option<DateTime<chrono::Local>> = Option::None;

    let mut api_call_timer_min: i32 = 0;

    loop {
        widget(&mut notified_state, &mut notified_stage, &mut next_loadshedding_start_time, &mut next_loadshedding_end_time, &mut api_call_timer_min);

        sleep(Duration::from_secs(u64::from(WIDGET_LOOP_PERIOD_S)));
    }
}

fn widget(notified_state: &mut LoadsheddingState, notified_stage: &mut Stage, next_loadshedding_start_time: &mut Option<DateTime<chrono::Local>>, next_loadshedding_end_time: &mut Option<DateTime<chrono::Local>>, api_call_timer_min: &mut i32) {
    let state: LoadsheddingState;

    // Make API call if API call timer has run out.
    if *api_call_timer_min <= 0 { // TODO: Can I do every 10 minutes? 
        api_call(notified_stage, next_loadshedding_start_time, next_loadshedding_end_time);

        *api_call_timer_min = i32::from(API_CALL_PERIOD_MIN);
    }

    let current_time: DateTime<chrono::Local> = chrono::Local::now();

    if *notified_stage == Stage::None {
        state = LoadsheddingState::None;
        if state != *notified_state {
            *notified_state = state;
            notify(&notified_state.get_tooltip_header(), &(String::from(ICONS_DIRECTORY) + &notified_state.get_icon_file()));
        }
        
        *api_call_timer_min -= 1;

        return;
    }

    let next_loadshedding_start_time: DateTime<chrono::Local> = next_loadshedding_start_time.expect("Error: Loadshedding start time not available");
    let next_loadshedding_end_time: DateTime<chrono::Local> = next_loadshedding_end_time.expect("Error: Loadshedding end time not available");

    let formatted_start_time: String = format!("{}", next_loadshedding_start_time.format("%H:%M"));
    if next_loadshedding_start_time < current_time { // Shedding a load.
        let time_remaining: TimeDelta = next_loadshedding_end_time - current_time;
        let minutes = (time_remaining.num_seconds() / 60) % 60;
        let hours = (time_remaining.num_seconds() / 60) / 60;
        state = LoadsheddingState::SheddingALoad(format!("{}", next_loadshedding_end_time.format("%H:%M")), String::from(format!("{:02}:{:02}", hours, minutes)));
    }
    else if next_loadshedding_start_time.checked_sub_signed(SOON).expect("Error checking 55 minutes") <= current_time { // Loadshedding in less than 55.
        let minutes = ((next_loadshedding_start_time - current_time).num_seconds() / 60) % 60;
        state = LoadsheddingState::Minutes(formatted_start_time, String::from(format!("00:{:02}", minutes)));
    }
    else if next_loadshedding_start_time.day() == current_time.day() { // Loadshedding later.
        state = LoadsheddingState::Later(formatted_start_time);
    }
    else if next_loadshedding_start_time.day() - current_time.day() == 1 { // Loadshedding tomorrow.
        state = LoadsheddingState::Tomorrow(formatted_start_time);
    }
    else { // Loadshedding a few days from now.
        state = LoadsheddingState::Days(next_loadshedding_start_time.weekday().to_string(), formatted_start_time);
    }

    // println!("{:?}", next_loadshedding_start_time);
    // println!("{:?}", current_time);

    if state != *notified_state {
        *notified_state = state;
        match notified_state {
            LoadsheddingState::Minutes(_, _) => notify("Loadshedding in 55 minutes", &(String::from(ICONS_DIRECTORY) + &notified_state.get_icon_file())),
            LoadsheddingState::SheddingALoad(_, _) => notify("Loadshedding has started", &(String::from(ICONS_DIRECTORY) + &notified_state.get_icon_file())),
            _ => notify(&notified_state.get_tooltip_header(), &(String::from(ICONS_DIRECTORY) + &notified_state.get_icon_file())),
        }
    }

    generate_output(&notified_state, &notified_stage);

    *api_call_timer_min -= 1;

    return;
}

fn api_call(notified_stage: &mut Stage, next_loadshedding_start_time: &mut Option<DateTime<chrono::Local>>, next_loadshedding_end_time: &mut Option<DateTime<chrono::Local>>) {
        let response: reqwest::Result<reqwest::blocking::Response> = reqwest::blocking::get(String::from(API_URL) + AREA_NAME);
        let body = match response {
            Ok(response) => match response.text() {
                Ok(text) => text,
                Err(_) => panic!("Response text error")
            },
            Err(_) => panic!("Response error")
        };
        // println!("{:?}", body);

        let loadsheddings: Vec<Loadshedding> = serde_json::from_str(&body).expect("Failed to deserialize JSON response.");
        // println!("{}", loadsheddings.len());

        let mut index: usize = 0;
        let mut start_time: DateTime<FixedOffset> = DateTime::UNIX_EPOCH.fixed_offset();
        let mut end_time: DateTime<FixedOffset> = DateTime::UNIX_EPOCH.fixed_offset();
        let stage: Stage;
        // If JSON array is empty, then there is no loadshedding ?
        if loadsheddings.len() == 0 {
            stage = Stage::None;
        }
        else {
            loop {
                if index >= loadsheddings.len() {
                    stage = Stage::None;
                    break;
                }

                let time_format: &str = "%Y-%m-%dT%H:%M:%S%z"; // 2024-02-18T22:00:00+02:00
                start_time = match DateTime::parse_from_str(&loadsheddings[index].start, time_format) {
                    Ok(date_time) => date_time,
                    Err(_) => panic!("Error parsing start time.")
                };
                end_time = match DateTime::parse_from_str(&loadsheddings[index].finsh, time_format) {
                    Ok(date_time) => date_time,
                    Err(_) => panic!("Error parsing end time.")
                };

                if end_time < chrono::Local::now() {
                    index += 1;
                }
                else {
                    stage = Stage::get(loadsheddings[index].stage);
                    break;
                }
            }
        }
        if *notified_stage != stage {
            *notified_stage = stage;
            if *notified_stage == Stage::None {
                notify("Loadshedding suspended", &(String::from(ICONS_DIRECTORY) + &notified_stage.get_icon_file()));
            }
            else {
                notify(&(String::from("Loadshedding stage ") + &notified_stage.ordinal().to_string() + " implemented"), &(String::from(ICONS_DIRECTORY) + &notified_stage.get_icon_file()));
            }
        }

        *next_loadshedding_start_time = Option::Some(start_time.with_timezone(&chrono::Local));
        *next_loadshedding_end_time = Option::Some(end_time.with_timezone(&chrono::Local));
}

fn generate_output(state: &LoadsheddingState, stage: &Stage) {
    // {"text": "\uf071 Tomorrow 04:00", "alt": "\uf242 Stage 2", "tooltip": "\t\t<span size=\"xx-large\">\uf071</span>\t\t\n\t\t\n<big>Loadshedding tomorrow at 04:00</big>\n\n<big>\uf242 Stage 2</big>", "class": "tomorrow"}
    let widget_out:Widget = Widget::from(state, stage);
    let json_out: String = serde_json::to_string(&widget_out).expect("Error serializing output to JSON.");

    println!("{}", json_out);
}

fn format_tooltip(icon: &str, tooltip_header: &str, tooltip_note: &str) -> String {
    return String::from("\t\t<span size=\"xx-large\">") + icon + "</span>\t\t\n\t\n" + "<big>" + tooltip_header + "</big>" + "\n\n" + "<big>" + tooltip_note + "</big>";
}

fn notify(title: &str, icon: &str) {
    Notification::new()
    .summary(title)
    .icon(icon)
    .timeout(notify_rust::Timeout::Never)
    // .timeout(notify_rust::Timeout::Milliseconds(10000)) 
    .show().unwrap();
}